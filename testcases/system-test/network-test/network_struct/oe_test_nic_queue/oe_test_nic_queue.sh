#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   wangdi
#@Contact   	:   15710801006@163.com
#@Date      	:   2023-10-25
#@License   	:   Mulan PSL v2
#@Desc      	:   Check nic queue
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    test_nic=$(TEST_NIC 1 | awk '{print $1}')
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    qn=$(ethtool -l "${test_nic}" | grep "Combined" | tail -n 1 | awk '{print $2}')
    test "$qn" -ge 1
    CHECK_RESULT $? 0 0 "The queue of ${test_nic} is less than 1."
    ethtool -S "${test_nic}" | grep "rx_queue" | grep -v "xdp" | grep "packets" | awk '{print $2}' >test_ori.log
    SLEEP_WAIT 10
    ethtool -S "${test_nic}" | grep "rx_queue" | grep -v "xdp" | grep "packets" | awk '{print $2}' >test_cur.log
    for i in $(seq 1 "$qn"); do
	test "$(sed -n "${i}"'p' test_ori.log)" -lt "$(sed -n "${i}"'p' test_cur.log)"
        CHECK_RESULT $? 0 0 "Check ${i} rx_queue failed."
    done
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    rm -rf test_ori.log test_cur.log
    LOG_INFO "End to restore the test environment."
}

main "$@"
