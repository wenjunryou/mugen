#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   aliceye666
# @Contact   :   yezhifen@uniontech.com
# @Date      :   2024-4-8
# @License   :   Mulan PSL v2
# @Desc      :   command lsmem
# ############################################
source "${OET_PATH}"/libs/locallibs/common_lib.sh

function run_test() {
    LOG_INFO "start to run test."
    lsmem -S RANGE | grep -i Memory 
    CHECK_RESULT $? 0 0 "with start and end address of the memory range to split"
    lsmem -S SIZE | grep -i Memory 
    CHECK_RESULT $? 0 0 "wirh size of the memory range to split"
    lsmem -S STATE | grep -i Memory
    CHECK_RESULT $? 0 0 "wirh online status of the memory range to split"
    lsmem -S REMOVABLE | grep -i Memory
    CHECK_RESULT $? 0 0 "wirh removable to split"
    lsmem -S BLOCK | grep -i Memory
    CHECK_RESULT $? 0 0 "wirh memory block number or blocks range to split"
    lsmem -S NODE | grep -i Memory
    CHECK_RESULT $? 0 0 "wirh numa node of memory to split"
    lsmem -S ZONES | grep -i Memory
    CHECK_RESULT $? 0 0 "wirh valid zones for the memory range to split"
    LOG_INFO "End to run test."
}

main "$@"
